## Cài đặt

Bước đầu tiên là cài đặt Rust. Chúng ta sẽ tải Rust thông qua `rustup`,
một công cụ dòng lệnh dành cho việc quản lý các phiên bản Rust và các công cụ liên quan.
Bạn sẽ cần kết nối internet trong lúc cài.

> Chú ý: Nếu bạn không thích sử dụng `rustup` vì một vài lý do nào đó, bạn có thể xem
> [những cách cài đặt khác][otherinstall].

[otherinstall]: https://forge.rust-lang.org/infra/other-installation-methods.html

Những bước sau đây cài đặt phiên bản stable mới nhất của trình biên dịch Rust.
Tính ổn định của Rust bảm đảm rằng tất cả các ví dụ biên dịch trong cuốn sách 
sẽ tiếp tục biên dịch với các phiên bản Rust mới hơn. Output có thể khác
một chút giữa các phiên bản, bởi Rust thường xuyên cải thiện các cảnh báo
và tin nhắn lỗi. Nói cách khác, bắt kì phiên bản ổn định mới hơn nào của Rust
mà bạn cài đặt bằng những bước này sẽ hoạt động đúng theo nội dung của cuốn sách này.

> ### Chú thích dòng lệnh
>
> Trong chương này và xuyên suốt cuốn sách, chúng tôi sẽ dùng một số  câu lệnh trong
> terminal. Những dòng mà bạn cần nhập trong terminal đều bắt đầu với `$`. Bạn
> không cần phải gõ ký tự `$`; nó biểu thị sự bắt đầu của mỗi câu lệnh.
> Những dòng không bắt đầu với `$` thường là biểu thị output của câu lệnh trước đó.
> Ngoài ra, ví dụ của PowerShell sẽ sử dụng `>` thay vì `$`.

### Cài đặt `rustup` trên Linux hay macOS

Nếu bạn đang sử dụng Linux hay macOS, mở terminal lên và nhập câu lệnh sau:

```console
$ curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh
```

Câu lệnh tải một đoạn mã và bắt đầu quá trình cài đặt công cụ `rustup`,
thứ sẽ cài đặt phiên bản ổn định mới nhất của Rust. Bạn có thể sẽ bị yêu cầu
nhập mật khẩu. Nếu quá trình cài đặt thành công, sẽ có thông báo sau hiện ra:

```text
Rust is installed now. Great!
```

Bạn cũng sẽ cần có linker, một chương trình được Rust sử dụng để kết nối các output đã
được biên dịch thành một file. Khả năng cao là các bạn đã có sẵn linker trong máy. Nếu bạn
gặp những lỗi về linker, bạn nên cài một trình biên dịch C, thứ đã bao gồm sẵn một linker.
Một trình biên dịch C cũng rất hữu ích bởi một số gói phổ biến của Rust phụ thuộc vào code
C và sẽ cần một trình biên dịch C.

Trên macOS, các bạn có thể cài trình biên dịch C với lệnh:

```console
$ xcode-select --install
```

Người dùng Linux nên cài đặt GCC hoặc Clang, tùy theo tài liệu của bản phân phối
(distribution) của họ. Ví dụ, nếu bạn dùng Ubuntu, bạn có thể cài gói
`build-essential`.

### Cài đặt `rustup` trên Windows

Đối với Windows, bạn đi tới [https://www.rust-lang.org/tools/install][install]
và làm theo hướng dẫn cài đặt Rust. Trong quá trình cài đặt, bạn sẽ nhận được
một thông báo nói rằng bạn sẽ cần C++ build tools for Visual Studio 2013 hoặc
mới hơn. Cách dễ nhất để có được những build tool này là cài đặt
[Build Tools for Visual Studio 2019][visualstudio]. Khi được hỏi về các mục muốn
cài đặt, hãy chắc rằng “C++ build tools” đã được chọn cùng với Windows 10 SDK và
gói ngôn ngữ.

[install]: https://www.rust-lang.org/tools/install
[visualstudio]: https://visualstudio.microsoft.com/visual-cpp-build-tools/

Phần còn lại của cuốn sách này sử dụng những câu lệnh có thể hoạt động cả
trong *cmd.exe* và PowerShell.
Nếu có khác, chúng tôi sẽ giải thích nên sử dụng cái nào.

### Cập nhật và Gỡ bỏ 

Sau khi bạn đã cài Rust thông qua `rustup`, bạn có thể dễ dàng cập nhật tới
phiên bản mới nhất. Trong shell của bạn, chạy đoạn mã cập nhật sau:

```console
$ rustup update
```

Để gỡ bỏ Rust và `rustup`, chay đoạn mã sau trong shell của bạn:

```console
$ rustup self uninstall
```

### Gỡ rối

Để kiểm tra liệu bạn đã cài Rust một cách chính xác hay chưa, mở shell lên và
nhập dòng sau:

```console
$ rustc --version
```

Bạn sẽ thấy số phiên bản, commit hash, và ngày commit của phiên bản ổn định mới
nhất đã được phát hành theo định dạng sau:

```text
rustc x.y.z (abcabcabc yyyy-mm-dd)
```

Nếu bạn nhìn thấy thông tin này, bạn đã cài đặt Rust thành công! Nếu không nhìn
thấy thông tin này và bạn đang trên Windows, kiểm tra xem Rust đã nằm trong biến
hệ thống `%PATH%` của bạn hay chưa. Nếu tất cả đã cài đặt đúng mà Rust vẫn không
làm việc, bạn có thể tìm sự trợ giúp ở một số diễn đàn. Dễ nhất là ở kênh #beginners
trên [Discord chính thức của Rust][discord]. Tại đó, bạn có thể chat với những
Rustaceans khác (nickname của chúng ta), họ có thể giúp bạn. Những nguồn hữu dụng khác
đó là [Rust users forum][users] và [Stack Overflow][stackoverflow].

[discord]: https://discord.gg/rust-lang
[users]: https://users.rust-lang.org/
[stackoverflow]: https://stackoverflow.com/questions/tagged/rust

### Tài liệu trong máy

Bản cài đặt của Rust đồng thời cũng chứa một bản sao của tài liệu, bạn có thể
đọc nó ngoại tuyến. Chạy `rustup doc` để mở tài liệu này trong trình duyệt của
bạn.

Bất cứ khi nào một kiểu hay hàm được cung cấp bởi thư viện chuẩn mà bạn
không chắc về chức năng hay cách sử dụng của nó, bạn có thể dùng tài liệu API
để tìm ra.
