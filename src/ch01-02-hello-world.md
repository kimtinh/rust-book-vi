## Hello, World!

Vậy là các bạn đã cài xong Rust, giờ thì thử viết chương trình Rust đầu tiên nào.
Cách truyền thống để học một ngôn ngữ mới là viết một chương trình nhỏ để in ra
chữ `Hello, world!` trên màn hình, ở đây chúng ta cũng sẽ làm tương tự!

> Chú ý: Cuốn sách này sử dụng dòng lệnh cơ bản. Nhưng Rust không có yêu cầu cụ
> thể gì về việc chỉnh sửa hay công cụ bạn dùng hay code chạy trên đâu, nên nếu
> bạn thích bạn có thể dùng IDE thay thế cho dòng lệnh. Có nhiều IDE hỗ trợ
> Rust; bạn có thể xem chi tiết trong tài liệu của IDE. Hiện nay, Rust team đang
> tập trung vào việc hỗ trợ IDE, và đang có tiến độ đáng kể!

### Tạo một thư mục dự án

Bắt đầu bằng việc tạo một thư mục để lưu trữ code Rust của bạn. Rust không quan
trọng code để đâu, nhưng với những ví dụ và dự án trong cuốn sách này, chúng
tôi khuyên bạn nên tạo một thư mục *projects* trong thư mục home của bạn và giữ
tất cả các project của bạn ở đó.

Mở terminal lên và nhập những lệnh sau để tạo một thư mục *projects* và một thư
mục cho project “Hello, world!” nằm trong thư mục *projects*.

Đối với Linux, macOS, và PowerShell trên Windows, nhập:

```console
$ mkdir ~/projects
$ cd ~/projects
$ mkdir hello_world
$ cd hello_world
```

Đối với Windows CMD, nhập:

```cmd
> mkdir "%USERPROFILE%\projects"
> cd /d "%USERPROFILE%\projects"
> mkdir hello_world
> cd hello_world
```

### Viết và chạy một chương trình Rust

Tiếp theo, tạo một file và đặt tên là *main.rs*. Những file Rust luôn kết thúc
với đuôi *.rs*. Nếu bạn có nhiều hơn một từ trong tên file, dùng dấu gạch dưới
để phân cách chúng. Ví dụ, đặt tên file là *hello_world.rs* hơn là *helloworld.rs*.

Giờ mở file *main.rs* vừa tạo ra và nhập đoạn code trong Listing 1-1.

<span class="filename">Filename: main.rs</span>

```rust
fn main() {
    println!("Hello, world!");
}
```

<span class="caption">Listing 1-1: Một chương trình in ra `Hello, world!`</span>

Lưu lại và quay về cửa sổ terminal. Trên Linux hoặc macOS, nhập những câu lệnh
sau để biên dịch và chạy file:

```console
$ rustc main.rs
$ ./main
Hello, world!
```

Trên Windows, nhập lệnh `.\main.exe` thay vì `./main`:

```powershell
> rustc main.rs
> .\main.exe
Hello, world!
```

Không quan trọng hệ điều hành của bạn là gì, chuỗi `Hello, world!` sẽ được in ra
terminal. Nếu bạn không nhìn thấy chuỗi này, xem lại mục [“Gỡ rối”][troubleshooting]
<!-- ignore --> trong phần Cài đặt.

Nếu `Hello, world!` được in ra, chúc mừng bạn! Bạn đã chính thức viết một chương
trình Rust và khiến bạn trở thành một tân lập trình viên Rust!

### Các thành phần của một chương trình Rust

Chúng ta hãy cùng bắt đầu xem xét một cách chi tiết điều gì đã xảy ra trong
chương trình “Hello, world!” của bạn.
Đây là mẩu đầu tiên của nó:

```rust
fn main() {

}
```

Những dòng này định nghĩa một hàm trong Rust. Hàm `main` là một hàm
đặc biệt: nó luôn là code đầu tiên được chạy trong mọi chương trình Rust mà thực
thi được. Dòng đầu tiên mô tả một hàm tên là `main` với không có tham số
(parameter) nào và không trả lại gì. Nếu có tham số , chúng sẽ nằm trong dấu
ngoặc đơn, `()`.

Thêm nữa, chú ý rằng thân của hàm được gói gọn trong dấu ngoặc nhọn, `{}`. Rust
yêu cầu dấu ngoặc nhọn bao quanh tất cả các thân hàm. Thường thì dấu mở ngoặc nên
được đặt cùng dòng với khai báo hàm, phân các bởi một dấu cách ở giữa.

Nếu bạn muốn gắn với một style chuẩn giữa các project, bạn có thể dùng một công cụ
định dạng tự động tên là `rustfmt` để định dạng code của bạn theo những style cụ thể.
Rust team đã đưa công cụ này vào bản cài chuẩn của Rust, giống như `rustc`, nên công cụ
này có thể đã có trong máy của bạn! Bạn có thể xem qua tài liệu trực tuyến để có những thông
tin chi tiết hơn.

Bên trong hàm `main` là đoạn code sau:

```rust
    println!("Hello, world!");
```

Dòng này thực hiện tất cả công việc của chương trình nhỏ này: nó in chữ ra màn
hình. Có 4 chi tiết quan trọng cần lưu ý ở đây.

Đầu tiên, phong cách của Rust là thụt đầu dòng với 4 dấu cách, chứ không phải 1 tab.

Thứ hai, `println!` gọi một Rust macro. Nếu nó gọi một hàm thì nó đã được viết
`println` (không có `!`). Chúng ta sẽ bàn về Rust macros chi tiết hơn ở Chương 19.
Bây giờ, bạn chỉ cần biết rằng sử dụng dấu `!` có nghĩa là bạn đang gọi một
macro thay vì một hàm bình thường, và macro không phải luôn tuân theo cùng quy 
tắc như hàm.

Thứ ba, bạn nhìn thấy chuỗi `"Hello, world!"`. Chúng ta truyền chuỗi này như một
đối số  tới `println!`, và chuỗi được in ra màn hình.

Thứ tư, chúng ta kết thúc dòng với dấu chấm phẩy (`;`), thứ chỉ ra sự diễn tả của
lệnh này đã kết thúc và lệnh tiếp theo đã sẵn sàng để bắt đầu. Hầu hết các dòng
code Rust đều kết thúc với dấu chấm phẩy.

### Biên dịch và chạy là hai bước riêng rẽ

Bạn chỉ vừa chạy một chương trình mới được tạo ra, hãy cùng xem xét từng bước
trong cả quá trình.

Trước khi chạy một chương trình Rust, bạn phải biên dịch nó với trình biên dịch
Rust bằng việc nhập lệnh `rustc` và truyền cho nó tên file nguồn, như thế này:

```console
$ rustc main.rs
```

Nếu bạn đã biết qua C hay C++, bạn sẽ để ý rằng nó tương tự như `gcc` hay `clang`.
Sau khi biên dịch thành công, Rust tạo ra một file nhị phân có thể thực thi.

Trên Linux, macOS và PowerShell trên Windows, bạn có thể nhìn thấy file thực thi
bằng lệnh `ls`. Trên Linux và macOS, bạn sẽ thấy hai file. Với PowerShell trên
Windows, bạn sẽ thấy ba file giống như những file bạn thấy trong CMD.

```console
$ ls
main  main.rs
```

Với CMD trên Windows, nhập lệnh sau:

```cmd
> dir /B %= tùy chọn /B làm kết quả chỉ hiện thị tên file =%
main.exe
main.pdb
main.rs
```

Kết quả trên hiển thị file mã nguồn với đuôi *.rs*, file thực thi (*main.exe*
trên Windows, *main* trên các nền tảng khác), và một file bao gồm thông tin debug
với đuôi *.pdb* trên Windows. Tiếp theo, bạn chạy file *main* hoặc *main.exe*
như sau:

```console
$ ./main # hoặc .\main.exe trên Windows
```

Nếu *main.rs* là chương trình “Hello, world!” của bạn, lệnh trên sẽ in `Hello,
world!` ra terminal của bạn.

Nếu bạn quên thuộc hơn với những ngôn ngữ động như Ruby, Python hay Javascript, bạn
có lẽ không thường xuyên biên dịch và chạy một chương trình như các bước riêng
rẽ. Rust là một ngôn ngữ biên dịch trước (*ahead-of-time compiled*), có nghĩa là
bạn có thể biên dịch một chương trình và đưa file thực thi cho người khác, họ
có thể chạy nó mà không cần phải cài đặt Rust. Nếu bạn đưa cho ai đó một file
*.rb*, *.py* hay *.js*, họ cần phải cài đặt Ruby, Python hay JavaScript trước.
Nhưng với những ngôn ngữ này, bạn chỉ cần một câu lệnh để biên dịch và chạy
chương trình. Chúng chỉ là một cách đánh đổi trong thiết kế ngôn ngữ.

Đối với chương trình đơn giản, chỉ biên dịch với `rustc` cũng ổn, nhưng khi
project của bạn phát triển lên, bạn sẽ muốn quản lý tất cả các tùy chọn và làm
cho việc chia sẻ nó dễ dàng hơn. Tiếp theo, chúng tôi sẽ giới thiệu cho bạn công
cụ Cargo, thứ sẽ giúp bạn viết những chương trình Rust thực sự ngoài đời.

[troubleshooting]: ch01-01-installation.html#gỡ-rối
