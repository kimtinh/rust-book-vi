# Hiều về Ownership

Ownership là tính năng độc đáo nhất của Rust, nó cho phép Rust đảm bảo an toàn cho
bộ nhớ mà không cần garbage collector. Do đó, hiểu về cách hoạt động của ownership
trong Rust rất quan trọng. Trong chương này, chúng ta sẽ nói về ownership cũng như
nhiều tính năng khác: borrowing, slice và cách Rust loại dữ liệu ra khỏi bộ nhớ.
