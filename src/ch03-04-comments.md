## Comments

Các lập trình viên cố gắng để làm cho code của họ dễ hiểu, nhưng đôi khi phải cần
thêm những đoạn giải thích. Trong những trường hợp này, lập trình viên sẽ để lại
*comment* trong source code của họ, thứ mà trình biên dịch sẽ bỏ qua nhưng người đọc
source code có thể thấy hữu dụng.

Ví dụ về một comment đơn giản:

```rust
// hello, world
```

Trong Rust, cách để bắt đầu một comment là dùng hai ký tự gạch chéo, và comment
sẽ được tính từ đó cho tới hết dòng. Với những comment dài nhiều dòng, bạn cần
thêm `//` cho từng dòng, như thế này:

```rust
// So we’re doing something complicated here, long enough that we need
// multiple lines of comments to do it! Whew! Hopefully, this comment will
// explain what’s going on.
```

Comment cũng có thể được đặt ở cuối dòng code:

<span class="filename">Filename: src/main.rs</span>

```rust
{{#rustdoc_include ../listings/ch03-common-programming-concepts/no-listing-24-comments-end-of-line/src/main.rs}}
```

Nhưng bạn sẽ thường thấy chúng hơn ở dạng đứng ở một dòng riêng phía trên dòng
code mà nó chú thích:

<span class="filename">Filename: src/main.rs</span>

```rust
{{#rustdoc_include ../listings/ch03-common-programming-concepts/no-listing-25-comments-above-line/src/main.rs}}
```

Rust còn có một loại comment khác, comment để làm tài liệu (documentation comment),
chúng ta sẽ nói về nó ở phần “Publishing a Crate to Crates.io” trong Chương 14.
