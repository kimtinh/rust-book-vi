# Những khái niệm lập trình cơ bản

Chương này đề cập đến những khái niệm xuất hiện trong hầu hết mọi ngôn ngữ lập
trình và cách chúng hoạt động trong Rust. Nhiều ngôn ngữ lập trình có
nhiều điểm cốt lõi chung. Không có khái niệm nào trong chương này là độc nhất
ở Rust, nhưng chúng ta sẽ thảo luận về chúng trong ngữ cảnh của Rust và giải
thích những quy ước xung quanh việc sử dụng những khái niệm này.

Đặc biệt, bạn sẽ học về biến, những kiểu cơ bản, hàm, comment và luồng điều
khiển. Những điều này sẽ nằm trong mọi chương trình Rust, và học chúng sớm sẽ
cho bạn một nền móng vững chắc để bắt đầu.

> #### Từ khóa
>
> Ngôn ngữ Rust có một bộ các *từ khóa* (*keyword*) được dành riêng và chỉ được
> sử dụng bới ngôn ngữ, giống như trong những ngôn ngữ khác. Hãy luôn nhớ rằng
> bạn không thể dùng những từ này cho tên biến hay tên hàm. Hầu hết các từ khóa
> có ý nghĩa đặc biệt, bạn sẽ dùng chúng để làm nhiều loại tác vụ trong chương
> trình Rust của bạn; một vài từ khóa hiện tại chưa có tính năng liên quan nhưng
> sẽ được sử dụng trong tương lai. Bạn có thể xem danh sách từ khóa ở
> [Phụ lục A][appendix_a]<!-- ignore -->.

[appendix_a]: appendix-01-keywords.md
