# Bắt đầu với Rust

Chúng ta hãy cùng bắt đầu hành trình với Rust của bạn nào! Có rất nhiều thứ để
học, nhưng mọi cuộc hành trình đều có điểm khởi đầu của nó. Trong chương này,
chúng ta sẽ thảo luận về:

* Cài đặt Rust trên Linux, macOS và Windows
* Viết một chương trình hiển thị ra `Hello, world!`
* Sử dụng `cargo`, trình quản lý gói và hệ thống build của Rust
